### Gaidīšanas laika aprēķināšana no Identu programmas faila

##### Lietošana:
1. Palaist powershell (WIN → rakstam: 'powershell' → peles labā poga uz 'Powershell' → 'Run As Admin')
2. Atvērt tajā mapi, kur atrodas Identu programmas rezultātu CSV fails  
   ``` cd c:\... ``` (ceļu var iekopēt no Windows Explorer)
3. Lejuplādēt skriptu:  
   ``` Invoke-WebRequest -OutFile .\gaidlaiks.ps1 -URI https://gitlab.com/api/v4/projects/43212011/repository/files/gaidlaiks.ps1/raw?ref=master ```
4. Atļaut powershell skriptu izpildi:  
   ``` Set-ExecutionPolicy unrestricted ```
5. Palaist skriptu, norādot CSV failu:  
   ``` .\gaidlaiks.ps1 -Path .\sime***.txt ```  
   Rezultāts saglabāsies tajā pašā mapē, failā ar nosaukumu "waiting-results.csv"