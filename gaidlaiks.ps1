param (
  [System.IO.FileInfo]$Path
)
$data = Get-Content $Path

function timeToSeconds ([string]$time) {
  [int32[]]$time = $time -split ":"
  $seconds = $time[0]*60*60 + $time[1]*60 + $time[2]
  return $seconds
}

$result = @()
foreach ($line in $data) {
  $i = 0
  $line = $line -split ";"
  $team_id = $line[0]
  $ident_id = $line[1]
  $line = $line[10..($line.Length)]
  $WaitingTotalInSeconds = 0
  while ($i+5 -lt $line.Length -and $line[$i+5] -ne "") {
    if ($line[$i] -eq $line[$i+3]) {
      $startTimeInSeconds = timeToSeconds($line[$i+2])
      $endTimeInSeconds = timeToSeconds($line[$i+5])
      $waitingInSeconds = $endTimeInSeconds - $startTimeInSeconds
      $WaitingTotalInSeconds += $waitingInSeconds
      $i = $i + 6
    } else {
      $i = $i + 3
    }
  }
  $result += [PSCustomObject]@{
    TEAM_ID = $team_id
    IDENT_ID = $ident_id
    WAITING = $([timespan]::fromseconds($WaitingTotalInSeconds))
  }
}

$result | Export-Csv -Path .\waiting-results.csv -Delimiter ";" -NoTypeInformation
$result